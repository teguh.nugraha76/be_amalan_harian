<?php

namespace App\Http\Controllers\Master\Kelas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Md_Kelas_Mata_Pelajaran;
use App\Tr_Penilaian;
use App\Md_Rombongan_Kelas;

class KelasMataPelajaran extends Controller
{

    protected $message_kelas_mata_pelajaran_success = null;
    protected $message_kelas_mata_pelajaran_error = null;
    protected $message_penilaian_success = null;
    protected $message_penilaian_error = null;


   public function get()
    {
        return response()->json([
            'status'  => 200,
            'message' => 'Success',
            'data'    => Md_Kelas_Mata_Pelajaran::with('kelas')->with('mataPelajaran')->get(),
        ], 200);
    }

    public function detail($id)
    {
        return response()->json([
            'status'  => 200,
            'message' => 'Success',
            'data'    => [
                'kelas' => Md_Kelas_Mata_Pelajaran::where('id_kelas', $id)->with('kelas')->first(),
                'mata_pelajaran' => Md_Kelas_Mata_Pelajaran::where('id_kelas', $id)->with('mataPelajaran')->with('kelas')->get(),
            ]
        ], 200);
    }

    public function create(Request $request)
    {

        $check_data = Md_Kelas_Mata_Pelajaran::where('id_kelas', $request->id_kelas)->where('id_mata_pelajaran', $request->id_mata_pelajaran)->first();

        if (!$check_data) {
            Md_Kelas_Mata_Pelajaran::create($request->all());
            $this->message_kelas_mata_pelajaran_success = 'data mata pelajaran berhasil di tambah';
        }
        else{
            $this->message_kelas_mata_pelajaran_error = 'data mata pelajaran gagal di tambah';
        }

        // add mata pelajaran on penilaian 
        $id_murid =  Md_Rombongan_Kelas::where('id_kelas', $request->id_kelas)->pluck('id_murid');
        $id_mata_pelajaran = Md_Kelas_Mata_Pelajaran::where('id_kelas', $request->id_kelas)->pluck('id_mata_pelajaran');

        foreach ($id_murid as $key => $value) {
            foreach ($id_mata_pelajaran as $key1 => $value1) {
                
                $check_mata_pelajaran_penilaian = Tr_Penilaian::where('id_murid', $id_murid[$key])->where('id_mata_pelajaran', $id_mata_pelajaran[$key1])->first();
                
                if (!$check_mata_pelajaran_penilaian) {
                    Tr_Penilaian::create([
                        'id_kelas' => $request->id_kelas,
                        'id_murid' => $id_murid[$key],
                        'id_mata_pelajaran' => $id_mata_pelajaran[$key1],
                    ]);
                    $this->message_penilaian_success[] = 'data mata pelajaran berhasil di tambah';
                }

            }
        }

        // return response
        return response()->json([
            'status' => 200,
            'message_kelas_mata_pelajaran' => [
                'success' => $this->message_kelas_mata_pelajaran_success,
                'error'   => $this->message_kelas_mata_pelajaran_error,
            ],
            'message_penilaian' => [
                'success' => $this->message_penilaian_success,
                'error'   => $this->message_penilaian_error,
            ],
        ], 200);

    }


    public function deleteMataPelajaran(Request $request)
    {
        $query_kelas_mata_pelajaran = Md_Kelas_Mata_Pelajaran::where('id_kelas', $request->id_kelas)->where('id_mata_pelajaran', $request->id_mata_pelajaran)->delete();
        $query_penilaian = Tr_Penilaian::where('id_kelas', $request->id_kelas)->where('id_mata_pelajaran', $request->id_mata_pelajaran)->delete();
        
        if ($query_kelas_mata_pelajaran) {
             $this->message_kelas_mata_pelajaran_success = 'data berhasil di hapus';
        } else {
             $this->message_kelas_mata_pelajaran_error = 'data gagal di hapus';
        }

        if ($query_penilaian) {
            $this->message_penilaian_success = 'data berhasil di hapus';
        } else {
            $this->message_kelas_mata_pelajaran_error = 'data gagal di hapus';
        }
        
        

        // return response
        return response()->json([
            'status' => 200,
            'message_kelas_mata_pelajaran' => [
                'success' => $this->message_kelas_mata_pelajaran_success,
                'error'   => $this->message_kelas_mata_pelajaran_error,
            ],
            'message_penilaian' => [
                'success' => $this->message_penilaian_success,
                'error'   => $this->message_penilaian_error,
            ],
        ], 200);
    }

}
