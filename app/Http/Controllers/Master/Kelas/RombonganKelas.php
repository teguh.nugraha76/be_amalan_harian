<?php

namespace App\Http\Controllers\Master\Kelas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Md_Rombongan_Kelas;
use App\Tr_Penilaian;
use App\Md_Kelas;
use App\Md_Kelas_Mata_Pelajaran;
use App\Md_Murid;

class RombonganKelas extends Controller
{

    protected $message_rombongan_kelas_success = null;
    protected $message_rombongan_kelas_error = null;
    protected $message_penilaian_success = null;
    protected $message_penilaian_error = null;


    // get all data kelas murid 
    public function get()
    {
        return response()->json([
            'status'  => 200,
            'message' => 'Success',
            'data'    => Md_Rombongan_Kelas::with('murid')->with('kelas')->get(),
        ], 200);
    }


    // get detail data kelas murid
    public function detail($id)
    {
        return response()->json([
            'status'  => 200,
            'message' => 'Success',
            'data'    => [
                'kelas' => Md_Rombongan_Kelas::where('id_kelas', $id)->with('kelas')->first(),
                'murid' => Md_Rombongan_Kelas::where('id_kelas', $id)->with('murid')->with('kelas')->get(),
            ]
        ], 200);
    }


    // create data kelas murid
    public function create(Request $request)
    {
        $check_mata_pelajaran   = Md_Kelas_Mata_Pelajaran::where('id_kelas', $request->id_kelas)->first();
        $mata_pelajaran         = Md_Kelas_Mata_Pelajaran::where('id_kelas', $request->id_kelas)->pluck('id_mata_pelajaran');
        $check_rombongan_kelas  = Md_Rombongan_Kelas::where('id_murid', $request->id_murid)->first();
        $check_penilaian        = Tr_Penilaian::where('id_murid', $request->id_murid)->first();
        $nama_murid             = Md_Murid::where('id_murid', $request->id_murid)->first()->nama;

        $data = [
            'id_kelas'   => $request->id_kelas,
            'id_murid'   => $request->id_murid,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ];

        // check if kelas have mata pelajaran or not 
        if ($check_mata_pelajaran) {
           
            // check and insert data kelas murid
            if (!$check_rombongan_kelas) {

                $query_rombongan_kelas = Md_Rombongan_Kelas::create($data);

                if ($query_rombongan_kelas) {
                    $this->message_rombongan_kelas_success[] = 'murid : ' . $nama_murid . ' berhasil di daftarkan';
                }
                else{
                    $this->message_rombongan_kelas_error[] = 'murid : ' . $nama_murid . ' gagal di daftarkan';
                }

            }
            else{

                $this->message_rombongan_kelas_error[] = 'murid : ' . $nama_murid . ' sudah terdaftar';

            }


            // check and insert data penilaian
            if (!$check_penilaian) {

                foreach ($mata_pelajaran as $key => $value) {
                    $query_penilaian = Tr_Penilaian::create( array_merge( $data, ['id_mata_pelajaran' => $value] ) );
                
                    if ($query_penilaian) {
                        $this->message_penilaian_success[] = 'murid : ' . $nama_murid . ' dengan id mata pelajaran ' . $value . ' berhasil di daftarkan';
                    }
                    else{
                        $this->message_penilaian_error[] = 'murid : ' . $nama_murid . ' dengan id mata pelajaran ' . $value . ' gagal di daftarkan';
                    }
                }

            }
            else{

                $this->message_penilaian_error[] = 'murid : ' . $nama_murid . ' sudah terdaftar';

            }

        }

        else{

            $this->message_rombongan_kelas_error = 'kelas harus terdaftar atau mempunyai setidaknya 1 mata pelajaran yang terdaftar';
        
        }

       // return response
        return response()->json([
            'status' => 200,
            'message_rombongan_kelas' => [
                'success' => $this->message_rombongan_kelas_success,
                'error'   => $this->message_rombongan_kelas_error,
            ],
            'message_penilaian' => [
                'success' => $this->message_penilaian_success,
                'error'   => $this->message_penilaian_error,
            ],
        ], 200);

    }


    // update kelas per murid
    public function updateKelasPerMurid(Request $request)
    {
        $nama_kelas = Md_Kelas::where('id_kelas', $request->id_kelas_baru)->first()->kelas;
        $nama_murid = Md_Murid::where('id_murid', $request->id_murid)->first()->nama;

        $data = [
            'id_murid' => $request->id_murid,
            'id_kelas' => $request->id_kelas_baru,
            'updated_at' => Carbon::now()->toDateTimeString(),
        ];


        // update kelas murid
        $query_rombongan_kelas = Md_Rombongan_Kelas::where('id_murid', $request->id_murid)->where('id_kelas', $request->id_kelas_lama)->update($data);
        
        if ($query_rombongan_kelas) {
            $this->message_rombongan_kelas_success = 'data murid : ' . $nama_murid . ', berhasil pindah ke kelas : ' . $nama_kelas;
        }
        else{
            $this->message_rombongan_kelas_error = 'data murid : ' . $nama_murid . ', gagal pindah ke kelas : ' . $nama_kelas;
        }

        // update penilaian
        Tr_Penilaian::where('id_murid', $request->id_murid)->where('id_kelas', $request->id_kelas_lama)->delete(); // delete semua data dengan id murid dan id kelas lama
        $mata_pelajaran = Md_Kelas_Mata_Pelajaran::where('id_kelas', $request->id_kelas_baru)->pluck('id_mata_pelajaran');

        foreach ($mata_pelajaran as $key => $value) {
            $query_penilaian = Tr_Penilaian::create( array_merge( $data, ['id_mata_pelajaran' => $value] ) );
            
            if ($query_penilaian) {
                $this->message_penilaian_success[] = 'data murid : ' . $nama_murid . ', berhasil pindah ke kelas : ' . $nama_kelas;
            }
            else{
                $this->message_penilaian_error[] = 'data murid : ' . $nama_murid . ', gagal pindah ke kelas : ' . $nama_kelas;
            }            
        }


        // return response
        return response()->json([
            'status' => 200,
            'message_rombongan_kelas' => [
                'success' => $this->message_rombongan_kelas_success,
                'error'   => $this->message_rombongan_kelas_error,
            ],
            'message_penilaian' => [
                'success' => $this->message_penilaian_success,
                'error'   => $this->message_penilaian_error,
            ],
        ], 200); 
    }


    // delete per kelas
    public function deleteKelas(Request $request)
    {
        $nama_kelas = Md_Kelas::where('id_kelas', $request->id_kelas)->first()->kelas;


        // delete kelas murid
        $query_rombongan_kelas = Md_Rombongan_Kelas::where('id_kelas', $request->id_kelas)->delete();
        
        if ($query_rombongan_kelas) {
            $this->message_rombongan_kelas_success = 'data dengan kelas : ' . $nama_kelas . ' berhasil di hapus';
        }
        else{
            $this->message_rombongan_kelas_error = 'data dengan kelas : ' . $nama_kelas . ' gagal di hapus';
        }


        // delete penilaian
        $query_penilaian   = Tr_Penilaian::where('id_kelas', $request->id_kelas)->delete();

        if ($query_penilaian) {
            $this->message_penilaian_success = 'data dengan kelas : ' . $nama_kelas . ' berhasil di hapus';
        }
        else{
            $this->message_penilaian_error = 'data dengan kelas : ' . $nama_kelas . ' gagal di hapus';
        }


        // return response
        return response()->json([
            'status' => 200,
            'message_rombongan_kelas' => [
                'success' => $this->message_rombongan_kelas_success,
                'error'   => $this->message_rombongan_kelas_error,
            ],
            'message_penilaian' => [
                'success' => $this->message_penilaian_success,
                'error'   => $this->message_penilaian_error,
            ],
        ], 200); 
    }


    // delete kelas per murid
    public function deleteMurid(Request $request)
    {
        $nama_kelas = Md_Kelas::where('id_kelas', $request->id_kelas)->first()->kelas;
        $nama_murid = Md_Murid::where('id_murid', $request->id_murid)->first()->nama;


        // delete kelas murid
        $query_rombongan_kelas = Md_Rombongan_Kelas::where('id_kelas', $request->id_kelas)->where('id_murid', $request->id_murid)->delete();
        
        if ($query_rombongan_kelas) {
            $this->message_rombongan_kelas_success = 'data dengan kelas : ' . $nama_kelas . ', dan murid : ' . $nama_murid . ' berhasil di hapus';
        }
        else{
            $this->message_rombongan_kelas_error = 'data dengan kelas : ' . $nama_kelas . ', dan murid : ' . $nama_murid . ' gagal di hapus';
        }


        // delete penilaian
        $query_penilaian   = Tr_Penilaian::where('id_kelas', $request->id_kelas)->where('id_murid', $request->id_murid)->delete();

        if ($query_penilaian) {
            $this->message_penilaian_success = 'data dengan kelas : ' . $nama_kelas . ', dan murid : ' . $nama_murid . ' berhasil di hapus';
        }
        else{
            $this->message_penilaian_error = 'data dengan kelas : ' . $nama_kelas . ', dan murid : ' . $nama_murid . ' gagal di hapus';
        }


        // return response
        return response()->json([
            'status' => 200,
            'message_rombongan_kelas' => [
                'success' => $this->message_rombongan_kelas_success,
                'error'   => $this->message_rombongan_kelas_error,
            ],
            'message_penilaian' => [
                'success' => $this->message_penilaian_success,
                'error'   => $this->message_penilaian_error,
            ],
        ], 200); 
    }

}
