<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Md_Rombongan_Kelas extends Model
{
    protected $table = 'md_rombongan_kelas';
    protected $fillable = ['id_kelas_murid', 'id_kelas', 'id_murid', 'created_at', 'updated_at'];
    protected $primaryKey = 'id_kelas_murid';
    public $timestamps = true;

    function murid() {
        return $this->hasOne(Md_Murid::class, 'id_murid', 'id_murid');
    }

    function kelas() {
        return $this->hasOne(Md_Kelas::class, 'id_kelas', 'id_kelas');
    }
}
