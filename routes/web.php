<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('register', 'AuthController@register');
$router->post('login', 'AuthController@login');
$router->get('logout', 'AuthController@logout');

$router->get('test', 'Master\Kelas@get');

$router->group(['middleware' => 'auth'], function () use ($router) {

    // =========================== MASTER DATA =========================== 
    
    ## -- MASTER KELAS --
    ## list kelas
    $router->get('master/kelas', 'Master\Kelas\Kelas@get');
    $router->get('master/kelas/{id}', 'Master\Kelas\Kelas@detail');
    $router->post('master/kelas/create', 'Master\Kelas\Kelas@create');
    $router->post('master/kelas/update/{id}', 'Master\Kelas\Kelas@update');
    $router->get('master/kelas/delete/{id}', 'Master\Kelas\Kelas@delete');

    ## kelas & murid
    $router->get('master/rombongan-kelas', 'Master\Kelas\RombonganKelas@get');
    $router->get('master/rombongan-kelas/{id}', 'Master\Kelas\RombonganKelas@detail');
    $router->post('master/rombongan-kelas/create', 'Master\Kelas\RombonganKelas@create');
    $router->post('master/rombongan-kelas/update-kelas-per-murid', 'Master\Kelas\RombonganKelas@updateKelasPerMurid');
    $router->post('master/rombongan-kelas/delete-kelas', 'Master\Kelas\RombonganKelas@deleteKelas');
    $router->post('master/rombongan-kelas/delete-murid', 'Master\Kelas\RombonganKelas@deleteMurid');

    ## mata pelajaran per kelas
    $router->get('master/kelas-mata-pelajaran', 'Master\Kelas\KelasMataPelajaran@get');
    $router->get('master/kelas-mata-pelajaran/{id}', 'Master\Kelas\KelasMataPelajaran@detail');
    $router->post('master/kelas-mata-pelajaran/create', 'Master\Kelas\KelasMataPelajaran@create');
    $router->post('master/kelas-mata-pelajaran/delete-mata-pelajaran', 'Master\Kelas\KelasMataPelajaran@deleteMataPelajaran');

    
    ## -- MASTER MATA PELAJARAN --
    ## list mata pelajaran
    $router->get('master/mata-pelajaran', 'Master\MataPelajaran\MataPelajaran@get');
    $router->get('master/mata-pelajaran/{id}', 'Master\MataPelajaran\MataPelajaran@detail');
    $router->post('master/mata-pelajaran/create', 'Master\MataPelajaran\MataPelajaran@create');
    $router->post('master/mata-pelajaran/update/{id}', 'Master\MataPelajaran\MataPelajaran@update');
    $router->get('master/mata-pelajaran/delete/{id}', 'Master\MataPelajaran\MataPelajaran@delete');

    ## list guru & mata pelajaran
    $router->get('master/guru-mata-pelajaran', 'Master\MataPelajaran\GuruMataPelajaran@get');
    $router->get('master/guru-mata-pelajaran/{id}', 'Master\MataPelajaran\GuruMataPelajaran@detail');
    $router->post('master/guru-mata-pelajaran/create', 'Master\MataPelajaran\GuruMataPelajaran@create');
    $router->post('master/guru-mata-pelajaran/update/{id}', 'Master\MataPelajaran\GuruMataPelajaran@update');
    $router->get('master/guru-mata-pelajaran/delete-guru/{id}', 'Master\MataPelajaran\GuruMataPelajaran@deleteGuru');
    $router->post('master/guru-mata-pelajaran/delete-mata-pelajaran', 'Master\MataPelajaran\GuruMataPelajaran@deleteMataPelajaran');


    ## -- MASTER MURID --
    ## master murid
    $router->get('master/murid', 'Master\Murid@get');
    $router->get('master/murid/{id}', 'Master\Murid@detail');
    $router->post('master/murid/create', 'Master\Murid@create');
    $router->post('master/murid/update/{id}', 'Master\Murid@update');
    $router->get('master/murid/delete/{id}', 'Master\Murid@delete');


    ## -- MASTER GURU --
    ## master guru
    $router->get('master/guru', 'Master\Guru@get');
    $router->get('master/guru/{id}', 'Master\Guru@detail');
    $router->post('master/guru/create', 'Master\Guru@create');
    $router->post('master/guru/update/{id}', 'Master\Guru@update');
    $router->get('master/guru/delete/{id}', 'Master\Guru@delete');


    ## -- MASTER TAHUN AJARAN --
    ## master tahun ajaran
    $router->get('master/tahun-ajaran', 'Master\TahunAjaran@get');
    $router->get('master/tahun-ajaran/{id}', 'Master\TahunAjaran@detail');
    $router->post('master/tahun-ajaran/create', 'Master\TahunAjaran@create');
    $router->post('master/tahun-ajaran/update/{id}', 'Master\TahunAjaran@update');
    $router->get('master/tahun-ajaran/delete/{id}', 'Master\TahunAjaran@delete');



    // =========================== PENILAIAN =========================== 
    $router->get('penilaian', 'Penilaian\Penilaian@get');
    $router->get('penilaian/kelas/{id}', 'Penilaian\Penilaian@kelas');
    $router->post('penilaian/siswa', 'Penilaian\Penilaian@siswa');
    $router->post('penilaian/update/{id}', 'Penilaian\Penilaian@update');



    // =========================== UASER MANAGEMENT =========================== 
    // user management User
    $router->get('user-management/user', 'UserManagement\Users@get');
    $router->get('user-management/user/{id}', 'UserManagement\Users@detail');
    $router->post('user-management/user/create', 'UserManagement\Users@create');
    $router->post('user-management/user/update/{id}', 'UserManagement\Users@update');
    $router->get('user-management/user/delete/{id}', 'UserManagement\Users@delete');

    // user management Role
    $router->get('user-management/role', 'UserManagement\Roles@get');
    $router->get('user-management/role/{id}', 'UserManagement\Roles@detail');
    $router->post('user-management/role/create', 'UserManagement\Roles@create');
    $router->post('user-management/role/update/{id}', 'UserManagement\Roles@update');
    $router->get('user-management/role/delete/{id}', 'UserManagement\Roles@delete');

});